/* Requerimientos: Se requiere que el usuario ingrese un numero romano.

  Garantiza: Convertir un número en el sistema romano al sistema arábigo usado actualmente.*/

#include <iostream>
#include <string.h>
#include <stdlib.h>

using namespace std;


int main()
{
    char numero[1000]={};
    char romanos[7]={'M','D','C','L','X','V','I'};
    int arabigo[7]={1000,500,100,50,10,5,1};
    int aux[1000]={};
    char *p_romano=romanos, *p_numeros=numero;
    int *p_arabigo=arabigo, *p_aux=aux;
    int cont=0;

    cout<<"Ingres el numero en romano (RECUERDE QUE DEBE SER EN MAYUSCULA O NO SE CONSIDERARA COMO UN NUMERO ROMANO) : ";
    cin>>numero;


    int l1=strlen(numero);//instruccion para obtener la longitud del numero romano ingresado.


    for(int i=0;i<l1;i++)//ciclo que me recorre el arreglo que el usuario ingreso.
    {
        for(int j=0;j<7;j++)//ciclo que me recorre los numeros romanos.
        {
            if(*(p_numeros+i)==*(p_romano+j))//condicional que me compara si la letra del numero romano coinciden con el arreglo romano ya definido.
            {
                *(p_aux+i)=*(p_arabigo+j);//Se llena el arreglo auxiliar con el numero romano ya cambiado a arabigo.
                //cout<<*(p_aux+i);

            }
        }
    }

    for(int i=0;i<l1-1;i++)//ciclo que me recorre las posiciones del arreglo auxiliar para mirar las condiciones, si se suman o se restan.
    {
        if(cont == 0){//si se cumple este condicional se lleva al contador la primera posicion del arreglo.
            cont = *(p_aux+i);
        }
        if(*(p_aux+(i+1))<=*(p_aux+i))//si se cumple este condicional al contador se le suma la siguiente posicion del arreglo.
        {
            cont+=*(p_aux+(i+1));

        }
        else {
            cont+= -2*(*(p_aux+i))+*(p_aux+(i+1));//de lo contrario se resta la siguiente posicion al contador

        }
    }
    cout<<"El numero ingresado fue: ";
    for(int i=0; i<l1; i++){//Ciclo para imprimir el arreglo del numero romano
        cout << *(p_numeros+i);
    }

    cout<<"\n Que corresponde a: " << cont <<endl;//Se imprime el numero romano en el sistema arabigo.

    return 0;

}
